{
	"contents": {
		"6ef34b5e-7dd8-4840-ad57-2d0a9fb8d6a4": {
			"classDefinition": "com.sap.bpm.wfs.Model",
			"id": "pcpworkflow",
			"subject": "Workflow Issue n° ${context.issue.ISSUE_ID}",
			"name": "pcpworkflow",
			"lastIds": "41ea3ed5-e5c3-4ee4-ad5e-61eed7a89dd6",
			"events": {
				"99f0041a-50df-46d9-acd5-2541d4768229": {
					"name": "StartEvent1"
				},
				"4f5c49ab-980f-43e8-99a8-e8f6e9f1b4dd": {
					"name": "EndEvent1"
				}
			},
			"activities": {
				"1a0cdb64-5fe2-42be-975c-649243ad3ed1": {
					"name": "Create Issue"
				},
				"d8be8ab6-11a0-4034-a152-4003e3d01d2a": {
					"name": "Assign TSA"
				},
				"03023921-ed9a-42b8-b934-9adb2a192c56": {
					"name": "Update TSA - on Issue"
				},
				"881a5b78-046f-4bc3-b116-5ebccf94389f": {
					"name": "Define Issue"
				},
				"771d8fe7-4e58-423c-b309-99062301b840": {
					"name": "Assign TEC"
				},
				"27443eef-1a60-4ec3-9ca4-0bc929baffb5": {
					"name": "Update TEC on Issue"
				}
			},
			"sequenceFlows": {
				"4b264c2a-3d90-4a9c-b891-953b90e8e58e": {
					"name": "SequenceFlow1"
				},
				"ba5b50ad-7943-4a9f-b39e-208ccd24cb14": {
					"name": "SequenceFlow2"
				},
				"aa34db3c-dc0a-4306-9feb-f931cec2d728": {
					"name": "SequenceFlow3"
				},
				"488fd3c3-0070-4111-87b1-2065dad07472": {
					"name": "SequenceFlow4"
				},
				"9d783bdd-0fd8-4ae1-b58c-6aa9f0ed911e": {
					"name": "SequenceFlow5"
				},
				"edbd82c9-9744-4fa1-8c98-b6426c2da9ae": {
					"name": "SequenceFlow6"
				},
				"2d2d9933-93a4-489f-b83b-bd83932cc65d": {
					"name": "SequenceFlow7"
				}
			},
			"diagrams": {
				"9c1c8285-d033-4615-81b0-977115799622": {}
			}
		},
		"99f0041a-50df-46d9-acd5-2541d4768229": {
			"classDefinition": "com.sap.bpm.wfs.StartEvent",
			"id": "startevent1",
			"name": "StartEvent1"
		},
		"4f5c49ab-980f-43e8-99a8-e8f6e9f1b4dd": {
			"classDefinition": "com.sap.bpm.wfs.EndEvent",
			"id": "endevent1",
			"name": "EndEvent1"
		},
		"1a0cdb64-5fe2-42be-975c-649243ad3ed1": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "pcp_enel_db_test",
			"path": "/pcp/pcptest_services.xsodata/issue",
			"httpMethod": "POST",
			"requestVariable": "${context.issue}",
			"responseVariable": "",
			"id": "servicetask1",
			"name": "Create Issue"
		},
		"d8be8ab6-11a0-4034-a152-4003e3d01d2a": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Select TSA for issue n° ${context.issue.ISSUE_ID}",
			"description": "",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/workflow/workflow",
			"recipientUsers": "P1942764673",
			"id": "usertask1",
			"name": "Assign TSA",
			"documentation": ""
		},
		"03023921-ed9a-42b8-b934-9adb2a192c56": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "pcp_enel_db_test",
			"path": "/pcp/pcptest_services.xsodata/issue('${context.issue.ISSUE_ID}')",
			"httpMethod": "PUT",
			"requestVariable": "${context.issue}",
			"responseVariable": "",
			"id": "servicetask2",
			"name": "Update TSA - on Issue"
		},
		"881a5b78-046f-4bc3-b116-5ebccf94389f": {
			"classDefinition": "com.sap.bpm.wfs.ScriptTask",
			"script": "var workflowInstanceId = $.info.workflowInstanceId;\n$.context.issue.WORKFLOWINSTANCEID = workflowInstanceId ; \n\n",
			"id": "scripttask1",
			"name": "Define Issue"
		},
		"771d8fe7-4e58-423c-b309-99062301b840": {
			"classDefinition": "com.sap.bpm.wfs.UserTask",
			"subject": "Select TEC for issue n° ${context.issue.ISSUE_ID}",
			"isHiddenInLogForParticipant": false,
			"userInterface": "sapui5://html5apps/workflowtech/workflowtech",
			"recipientUsers": "P1942768096",
			"id": "usertask2",
			"name": "Assign TEC"
		},
		"27443eef-1a60-4ec3-9ca4-0bc929baffb5": {
			"classDefinition": "com.sap.bpm.wfs.ServiceTask",
			"destination": "pcp_enel_db_test",
			"path": "/pcp/pcptest_services.xsodata/issue('${context.issue.ISSUE_ID}')",
			"httpMethod": "PUT",
			"requestVariable": "${context.issue}",
			"responseVariable": "",
			"id": "servicetask3",
			"name": "Update TEC on Issue"
		},
		"4b264c2a-3d90-4a9c-b891-953b90e8e58e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow1",
			"name": "SequenceFlow1",
			"sourceRef": "99f0041a-50df-46d9-acd5-2541d4768229",
			"targetRef": "881a5b78-046f-4bc3-b116-5ebccf94389f"
		},
		"ba5b50ad-7943-4a9f-b39e-208ccd24cb14": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow2",
			"name": "SequenceFlow2",
			"sourceRef": "1a0cdb64-5fe2-42be-975c-649243ad3ed1",
			"targetRef": "d8be8ab6-11a0-4034-a152-4003e3d01d2a"
		},
		"aa34db3c-dc0a-4306-9feb-f931cec2d728": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow3",
			"name": "SequenceFlow3",
			"sourceRef": "d8be8ab6-11a0-4034-a152-4003e3d01d2a",
			"targetRef": "03023921-ed9a-42b8-b934-9adb2a192c56"
		},
		"488fd3c3-0070-4111-87b1-2065dad07472": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow4",
			"name": "SequenceFlow4",
			"sourceRef": "03023921-ed9a-42b8-b934-9adb2a192c56",
			"targetRef": "771d8fe7-4e58-423c-b309-99062301b840"
		},
		"9d783bdd-0fd8-4ae1-b58c-6aa9f0ed911e": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow5",
			"name": "SequenceFlow5",
			"sourceRef": "881a5b78-046f-4bc3-b116-5ebccf94389f",
			"targetRef": "1a0cdb64-5fe2-42be-975c-649243ad3ed1"
		},
		"edbd82c9-9744-4fa1-8c98-b6426c2da9ae": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow6",
			"name": "SequenceFlow6",
			"sourceRef": "771d8fe7-4e58-423c-b309-99062301b840",
			"targetRef": "27443eef-1a60-4ec3-9ca4-0bc929baffb5"
		},
		"2d2d9933-93a4-489f-b83b-bd83932cc65d": {
			"classDefinition": "com.sap.bpm.wfs.SequenceFlow",
			"id": "sequenceflow7",
			"name": "SequenceFlow7",
			"sourceRef": "27443eef-1a60-4ec3-9ca4-0bc929baffb5",
			"targetRef": "4f5c49ab-980f-43e8-99a8-e8f6e9f1b4dd"
		},
		"9c1c8285-d033-4615-81b0-977115799622": {
			"classDefinition": "com.sap.bpm.wfs.ui.Diagram",
			"symbols": {
				"b38ecb17-c4a4-4ed9-84e0-45db0fe587d8": {},
				"333bb52e-4053-4dc1-b4cf-9f13f756be1b": {},
				"35ed142a-96b8-41cc-9bd3-6851f7d5dbf1": {},
				"3affa012-8fa0-4cc9-81c4-b101627f56be": {},
				"26e244ad-62b2-4495-b966-5e84f7b1431e": {},
				"eadefc17-f3c2-4422-bd85-9ef6184d48cc": {},
				"c89911d3-e6bf-4727-b278-fdfb07761d5b": {},
				"f0714d52-be2c-4bb4-98dc-fd72fb37bc83": {},
				"42c0909a-6619-436b-9b3c-13dfaf0e55c1": {},
				"7bbc5537-da26-4022-abc7-e15af17d971d": {},
				"6ffbda8f-feaa-4876-b87c-b0f279a7e266": {},
				"5af04671-e819-4105-b209-fa687dc13148": {},
				"1daa4142-301c-49a1-8c2a-9879816adc12": {},
				"99063b8d-3e70-4704-8ad8-78a1eaabb23f": {},
				"1f6032d1-3f17-4718-8b2d-43b028c86885": {}
			}
		},
		"b38ecb17-c4a4-4ed9-84e0-45db0fe587d8": {
			"classDefinition": "com.sap.bpm.wfs.ui.StartEventSymbol",
			"x": -36,
			"y": 100,
			"width": 32,
			"height": 32,
			"object": "99f0041a-50df-46d9-acd5-2541d4768229"
		},
		"333bb52e-4053-4dc1-b4cf-9f13f756be1b": {
			"classDefinition": "com.sap.bpm.wfs.ui.EndEventSymbol",
			"x": 934,
			"y": 100,
			"width": 32,
			"height": 32,
			"object": "4f5c49ab-980f-43e8-99a8-e8f6e9f1b4dd"
		},
		"35ed142a-96b8-41cc-9bd3-6851f7d5dbf1": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "-20,116 96,116",
			"sourceSymbol": "b38ecb17-c4a4-4ed9-84e0-45db0fe587d8",
			"targetSymbol": "7bbc5537-da26-4022-abc7-e15af17d971d",
			"object": "4b264c2a-3d90-4a9c-b891-953b90e8e58e"
		},
		"3affa012-8fa0-4cc9-81c4-b101627f56be": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 179,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "1a0cdb64-5fe2-42be-975c-649243ad3ed1"
		},
		"26e244ad-62b2-4495-b966-5e84f7b1431e": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "229,116 372,116",
			"sourceSymbol": "3affa012-8fa0-4cc9-81c4-b101627f56be",
			"targetSymbol": "eadefc17-f3c2-4422-bd85-9ef6184d48cc",
			"object": "ba5b50ad-7943-4a9f-b39e-208ccd24cb14"
		},
		"eadefc17-f3c2-4422-bd85-9ef6184d48cc": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 322,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "d8be8ab6-11a0-4034-a152-4003e3d01d2a"
		},
		"c89911d3-e6bf-4727-b278-fdfb07761d5b": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "372,116 517,116",
			"sourceSymbol": "eadefc17-f3c2-4422-bd85-9ef6184d48cc",
			"targetSymbol": "f0714d52-be2c-4bb4-98dc-fd72fb37bc83",
			"object": "aa34db3c-dc0a-4306-9feb-f931cec2d728"
		},
		"f0714d52-be2c-4bb4-98dc-fd72fb37bc83": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 467,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "03023921-ed9a-42b8-b934-9adb2a192c56"
		},
		"42c0909a-6619-436b-9b3c-13dfaf0e55c1": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "517,116 654,116",
			"sourceSymbol": "f0714d52-be2c-4bb4-98dc-fd72fb37bc83",
			"targetSymbol": "5af04671-e819-4105-b209-fa687dc13148",
			"object": "488fd3c3-0070-4111-87b1-2065dad07472"
		},
		"7bbc5537-da26-4022-abc7-e15af17d971d": {
			"classDefinition": "com.sap.bpm.wfs.ui.ScriptTaskSymbol",
			"x": 46,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "881a5b78-046f-4bc3-b116-5ebccf94389f"
		},
		"6ffbda8f-feaa-4876-b87c-b0f279a7e266": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "96,116 229,116",
			"sourceSymbol": "7bbc5537-da26-4022-abc7-e15af17d971d",
			"targetSymbol": "3affa012-8fa0-4cc9-81c4-b101627f56be",
			"object": "9d783bdd-0fd8-4ae1-b58c-6aa9f0ed911e"
		},
		"5af04671-e819-4105-b209-fa687dc13148": {
			"classDefinition": "com.sap.bpm.wfs.ui.UserTaskSymbol",
			"x": 604,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "771d8fe7-4e58-423c-b309-99062301b840"
		},
		"1daa4142-301c-49a1-8c2a-9879816adc12": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "654,116 826,116",
			"sourceSymbol": "5af04671-e819-4105-b209-fa687dc13148",
			"targetSymbol": "99063b8d-3e70-4704-8ad8-78a1eaabb23f",
			"object": "edbd82c9-9744-4fa1-8c98-b6426c2da9ae"
		},
		"99063b8d-3e70-4704-8ad8-78a1eaabb23f": {
			"classDefinition": "com.sap.bpm.wfs.ui.ServiceTaskSymbol",
			"x": 776,
			"y": 88.5,
			"width": 100,
			"height": 55,
			"object": "27443eef-1a60-4ec3-9ca4-0bc929baffb5"
		},
		"1f6032d1-3f17-4718-8b2d-43b028c86885": {
			"classDefinition": "com.sap.bpm.wfs.ui.SequenceFlowSymbol",
			"points": "826,116 950,116",
			"sourceSymbol": "99063b8d-3e70-4704-8ad8-78a1eaabb23f",
			"targetSymbol": "333bb52e-4053-4dc1-b4cf-9f13f756be1b",
			"object": "2d2d9933-93a4-489f-b83b-bd83932cc65d"
		},
		"41ea3ed5-e5c3-4ee4-ad5e-61eed7a89dd6": {
			"classDefinition": "com.sap.bpm.wfs.LastIDs",
			"sequenceflow": 12,
			"startevent": 1,
			"endevent": 1,
			"servicetask": 3,
			"usertask": 2,
			"scripttask": 5
		}
	}
}